<?php
/**
 * @file
 * uw_cfg_feds_search.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_cfg_feds_search_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: user_permission
  $overrides["user_permission.administer search.roles|anonymous user"] = 'anonymous user';
  $overrides["user_permission.administer search.roles|authenticated user"] = 'authenticated user';
  $overrides["user_permission.search content.roles|anonymous user"] = 'anonymous user';
  $overrides["user_permission.search content.roles|authenticated user"] = 'authenticated user';

 return $overrides;
}
