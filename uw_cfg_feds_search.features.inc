<?php
/**
 * @file
 * uw_cfg_feds_search.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_cfg_feds_search_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_user_default_permissions_alter().
 */
function uw_cfg_feds_search_user_default_permissions_alter(&$data) {
  if (isset($data['administer search'])) {
    $data['administer search']['roles']['anonymous user'] = 'anonymous user'; /* WAS: '' */
    $data['administer search']['roles']['authenticated user'] = 'authenticated user'; /* WAS: '' */
  }
  if (isset($data['search content'])) {
    $data['search content']['roles']['anonymous user'] = 'anonymous user'; /* WAS: '' */
    $data['search content']['roles']['authenticated user'] = 'authenticated user'; /* WAS: '' */
  }
}
