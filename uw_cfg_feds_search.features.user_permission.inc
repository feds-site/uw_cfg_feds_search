<?php
/**
 * @file
 * uw_cfg_feds_search.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_cfg_feds_search_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'admin node search exlusions'.
  $permissions['admin node search exlusions'] = array(
    'name' => 'admin node search exlusions',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'search_config',
  );

  // Exported permission: 'search all content'.
  $permissions['search all content'] = array(
    'name' => 'search all content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'search_config',
  );

  // Exported permission: 'search all excluded entities'.
  $permissions['search all excluded entities'] = array(
    'name' => 'search all excluded entities',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'search_config',
  );

  // Exported permission: 'search contact content'.
  $permissions['search contact content'] = array(
    'name' => 'search contact content',
    'roles' => array(),
    'module' => 'search_config',
  );

  // Exported permission: 'search schemaorg_event content'.
  $permissions['search schemaorg_event content'] = array(
    'name' => 'search schemaorg_event content',
    'roles' => array(),
    'module' => 'search_config',
  );

  // Exported permission: 'search uw_event content'.
  $permissions['search uw_event content'] = array(
    'name' => 'search uw_event content',
    'roles' => array(),
    'module' => 'search_config',
  );

  // Exported permission: 'search uw_web_form content'.
  $permissions['search uw_web_form content'] = array(
    'name' => 'search uw_web_form content',
    'roles' => array(),
    'module' => 'search_config',
  );

  // Exported permission: 'search uwaterloo_custom_listing content'.
  $permissions['search uwaterloo_custom_listing content'] = array(
    'name' => 'search uwaterloo_custom_listing content',
    'roles' => array(),
    'module' => 'search_config',
  );

  // Exported permission: 'search webform content'.
  $permissions['search webform content'] = array(
    'name' => 'search webform content',
    'roles' => array(),
    'module' => 'search_config',
  );

  return $permissions;
}
