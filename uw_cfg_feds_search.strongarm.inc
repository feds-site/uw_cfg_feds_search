<?php
/**
 * @file
 * uw_cfg_feds_search.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_cfg_feds_search_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uw_ct_project_search_enable';
  $strongarm->value = '1';
  $export['uw_ct_project_search_enable'] = $strongarm;

  return $export;
}
